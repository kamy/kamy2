package hk.com.hkex.mtl.nete.nete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NeteApplication {

	public static void main(String[] args) {
		SpringApplication.run(NeteApplication.class, args);
	}
}
